#[macro_use]
extern crate lazy_static;

pub mod audio {
    #[allow(dead_code)]
    use macroquad::audio::{
        load_sound, play_sound, set_sound_volume, stop_sound, PlaySoundParams, Sound,
    };
    use macroquad::prelude::*;
    use std::{collections::HashMap, sync::Mutex};

    lazy_static! {
        static ref AUDIO_CACHE: Mutex<HashMap<String, SoundChunk>> = Mutex::new(HashMap::new());
    }

    #[allow(dead_code)]
    pub struct AudioManager;

    #[derive(Debug, Copy, Clone)]
    pub struct SoundChunk {
        pub volume: f32,
        pub looped: bool,
        pub sound: Sound,
    }

    impl AudioManager {
        pub async fn new(name: &str, file_uri: &str, volume: f32, looped: bool) -> SoundChunk {
            if name == "" {
                panic!("You have not given a name for audio file - {:?}", file_uri);
            }

            let sound = load_sound(file_uri).await.unwrap_or_else(|error| {
                panic!("Problem whilst loading file. Error Type - {:?}", error);
            });

            let new_sound = SoundChunk {
                volume,
                looped,
                sound,
            };

            AUDIO_CACHE
                .lock()
                .unwrap()
                .insert(String::from(name), new_sound);

            new_sound
        }

        pub fn mute_by_suffix(suffix: &str) {
            let audio_cache = AUDIO_CACHE.lock().unwrap();
            for (key, _value) in &*audio_cache {
                if key.contains(suffix) {
                    set_sound_volume(audio_cache.get(key).unwrap().sound, 0.);
                }
            }
        }

        pub fn unmute_by_suffix(suffix: &str) {
            let audio_cache = AUDIO_CACHE.lock().unwrap();
            for (key, _value) in &*audio_cache {
                if key.contains(suffix) {
                    set_sound_volume(audio_cache.get(key).unwrap().sound, 1.);
                }
            }
        }

        pub fn set_volume(name: &str, val: f32) {
            let audio_cache = AUDIO_CACHE.lock().unwrap();
            let sound_from_cache = audio_cache.get(name);
            set_sound_volume(sound_from_cache.unwrap().sound, val);
        }

        pub fn clear_cache() {
            let mut audio_cache = AUDIO_CACHE.lock().unwrap();
            audio_cache.clear();
        }

        pub fn play(name: &str) {
            if name == "" {
                panic!("You have not given a name for audio file");
            }

            let audio_cache = AUDIO_CACHE.lock().unwrap();
            let sound_from_cache = audio_cache.get(name);

            play_sound(
                sound_from_cache.unwrap().sound,
                PlaySoundParams {
                    looped: sound_from_cache.unwrap().looped,
                    volume: sound_from_cache.unwrap().volume,
                },
            );
        }

        pub fn stop(name: &str) {
            if name == "" {
                panic!("You have not given a name for audio file");
            }

            let audio_cache = AUDIO_CACHE.lock().unwrap();
            let sound_from_cache = audio_cache.get(name);

            stop_sound(sound_from_cache.unwrap().sound);
        }

        pub fn get_sound(name: &str) -> SoundChunk {
            if name == "" {
                panic!("You have not given a name for audio file");
            }

            let audio_cache = AUDIO_CACHE.lock().unwrap();
            let sound_from_cache = audio_cache.get(name);

            *sound_from_cache.unwrap()
        }

        pub async fn play_crossfade(fadeout_speed: f32, fadein_speed: f32, from: &str, to: &str) {
            let current_sound = AudioManager::get_sound(from);
            let next_sound = AudioManager::get_sound(to);

            AudioManager::play(to);

            let mut current_sound_volume = current_sound.volume;
            let mut next_sound_volume = 0.;
            loop {
                current_sound_volume -= fadeout_speed * get_frame_time();
                next_sound_volume += fadein_speed * get_frame_time();

                if current_sound_volume <= 0. {
                    current_sound_volume = 0.;
                }

                #[allow(unused_assignments)]
                if next_sound_volume >= 1. {
                    next_sound_volume = 1.;
                    break;
                }

                set_sound_volume(current_sound.sound, current_sound_volume);
                set_sound_volume(next_sound.sound, next_sound_volume);

                next_frame().await;
            }
        }
    }
}
